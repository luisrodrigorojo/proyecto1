package test.test;
import org.apache.log4j.Logger;
/**
 * Hello world!
 *
 */
public class App 
{
		
	private static Logger logger = Logger.getLogger(App.class);
	public static void main( String[] args ) {
		logger.debug("DEBUG");
		logger.info("INFO");
		logger.warn("WARNING");
		logger.error("ERROR");
		logger.fatal("FATAL");
        System.out.println( "Hello World!" );
    }
}
